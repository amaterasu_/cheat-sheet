Install Arch Linux
==================

This is my personal setup almost every time I install Arch Linux.
This is meant for me as being something I can quickly reference.

Change localkeys to my keyboard layout
--------------------------------------

You can find your keymap file by using localectl:

```bash
localectl list-keymaps
```


In my case I have a Serbian keyboard layout.

```bash
loadkeys sr-latin
```

Create Partitions
-----------------

To list block devices :
```bash
lsblk
```
Now, select a device to partition

```bash
cfdisk /dev/DEVICE
```

Set up the partions, mount and use swap
---------------------------------------

Let us say you have a simple setup:

- /boot/efi     partition (vfat)
- /             partition (ext4)
- /home         partition (ext4)


NOTE: `lsblk` is a very nice tool to doublecheck your partition(s) structure.

First we format to the file systems we want:

```bash
mkfs.fat -F32 /dev/PART (/boot/efi)
mkfs.ext4 /dev/PART (/, /home)
```

And then mount the file systems to /mnt:

```bash
# Mount / (root)
mount /dev/sdaxY /mnt
```
Create /mnt structure, one-liner:

```bash
mkdir -p /mnt/{boot,home}
```

```bash
# Mount /boot, /home
mount /dev/PART /mnt/<point>
```


Installing the actual base system and doas
------------------------------------------

Install the base system, with some packages I like as well.

```bash
pacstrap /mnt base base-devel opendoas neovim iwd dhcpcd linux linux-headers linux-firmware
```

Generate the fstab (so it knows how things are mounted)
-------------------------------------------------------

This is why we had to mount everything as first, so genfstab
would know what to do when generating the fstab.

```bash
genfstab -U -p /mnt >> /mnt/etc/fstab
```

chroot into the installed system with /bin/bash
-----------------------------------------------

```bash
arch-chroot /mnt /bin/bash
```

Locale (system language)
------------------------

Depending on what you want, you change the /etc/locale.gen file:

```bash
nvim /etc/locale.gen
```

NOTE: Use UTF-8 whenever possible.

DOUBLE NOTE: If you are from America, you don't need to change the file.

When done, you simply generate the locales you want and tell the system what you
want to use.

Generate:

```bash
locale-gen
```

Tell the system what we use:

```bash
echo LANG=en_US.UTF-8 > /etc/locale.conf
export LANG=en_US.UTF-8
```

Timezone
--------

Symlink/Set your timezone:

```bash
ln -s /usr/share/zoneinfo/CONTINENT/CITY /etc/localtime
```

Set system to the Hardware Clock
--------------------------------

```bash
hwclock --systohc --local
```

Hostname
--------

Choose an awesome hostname:

```bash
echo myhostname > /etc/hostname
```

Edit /etc/vconsole.conf:

```bash
KEYMAP=sr-latin
```

Add matching entries to hosts:

```bash
127.0.0.1	localhost
::1		    localhost
127.0.1.1	myhostname.localdomain	myhostname
```


Ramdisk
-------

Initial ramdisk environment.

```bash
mkinitcpio -P
```

This actually ran under the pacstrap process, I just like to be safe.

Root Password
-------------

Be stronk:

```bash
passwd
```

Grub (Bootloader)
-----------------

Well, we need a bootloader:

```bash
pacman -S refind
```
It's as simple as

```bash
refind-install
```

Also, it will break on first boot. You will have to find a way to boot without it and do refind-install again within the system.


Exit arch-chroot
----------------

```bash
exit
```

Umount /mnt and reboot
----------------------

Unmount EVERYTHING and reboot the system.

```bash
umount -R /mnt
reboot
```


Login as Root to Arch Linux and permantly set the keymap (keyboard)
-------------------------------------------------------------------

Now we want to make our keyboard layout permanent:

```bash
localectl set-keymap --no-convert sr-latin
```


Add User and set Password
-------------------------

```bash
useradd -m -g users -G audio,video,network,games,wheel -s /bin/bash username
passwd username
```

Change sudoers file using nvim
------------------------------

```bash
EDITOR=nvim visudo
```

Uncomment wheel group.

`# %wheel ALL=(ALL:ALL) ALL`

Enable services :
-----------------

```bash
systemctl enable --now dhcpcd iwd

```


Logout of Root
--------------

```bash
exit
```

Login as your username and test sudo with pacman
------------------------------------------------

```bash
sudo pacman -Syyu  ...
```
